import Vue from 'vue'
import Vuex from 'vuex'
import StocksModule from './modules/stocks'
import PortfoliosModule from './modules/portfolio'
import * as types from './types'
import { db } from '@/db/firebase'
import { firebaseAction } from 'vuexfire'

Vue.use(Vuex)

const fundsKey = '1'

export default new Vuex.Store({
  state: {
    [types.FUNDS]: 10000
  },
  getters: {
    [types.GET_FUNDS] (state: any) {
      return state[types.FUNDS]
    }
  },
  mutations: {
    [types.SET_FUNDS] (state: any, funds: number) {
      state[types.FUNDS] = funds
    }
  },
  actions: {
    [types.UPDATE_FUNDS] (context: any, amount: number) {
      const funds = context.state[types.FUNDS] + amount

      context.commit(types.SET_FUNDS, funds)
    },
    [types.SAVE_FUNDS]: firebaseAction((context: any) => {
      const funds = context.state[types.FUNDS]

      db.collection('funds')
        .doc(fundsKey)
        .set({ [types.FUNDS]: funds })
        .then()
    }),
    [types.QUERY_FUNDS] (context) {
      db.collection('funds')
        .doc(fundsKey)
        .get()
        .then(doc => {
          context.commit(types.SET_FUNDS, doc.data()![types.FUNDS])
        })
    }
  },
  modules: {
    StocksModule,
    PortfoliosModule
  }
})
