import * as types from '../types'
import { Stock } from '@/models/stock.model'

const state = {
  [types.STOCKS]: [
    {
      label: 'Jumia',
      price: 45
    },
    {
      label: 'Apple',
      price: 150
    },
    {
      label: 'Google',
      price: 175
    },
    {
      label: 'Twitter',
      price: 75
    }
  ] as Stock[]
}

const getters = {
  [types.GET_STOCKS] (state: any) {
    return state[types.STOCKS]
  },
  [types.GET_STOCK] (state: any, payload: string) {
    return state[types.STOCKS].find((s: Stock) => s.label === payload)
  }
}

const mutations = {
  [types.SET_STOCKS] (state: any, payload: Stock[]) {
    state[types.STOCKS] = [...payload]
  }
}

const actions = {
  [types.END_DAY] (context: any) {
    const stocks = context.state[types.STOCKS].map((s: Stock) => {
      return {
        label: s.label,
        price: generateRandomPrice(10, 500)
      }
    })

    context.commit(types.SET_STOCKS, stocks)
  }
}

const generateRandomPrice = (start: number, end: number) =>
  Math.floor(Math.random() * end) + start

export default {
  state,
  getters,
  mutations,
  actions
}
