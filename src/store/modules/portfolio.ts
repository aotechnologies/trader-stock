import * as types from '../types'
import { PortfolioItem } from '@/models/portfolio-item.model'
import { Stock } from '@/models/stock.model'
import { db } from '@/db/firebase'
import { firestoreAction, firebaseAction } from 'vuexfire'

const portfolioKey = 'ma-collection-privee'

const state = {
  [types.PORTFOLIO]: [] as PortfolioItem[]
}

const getters = {
  [types.GET_PORTFOLIO] (state: any): PortfolioItem[] {
    return state[types.PORTFOLIO].filter((pi: PortfolioItem) => pi.quantity > 0)
  }
}

const mutations = {
  [types.SET_PORTFOLIO] (state: any, payload: PortfolioItem[]) {
    state[types.PORTFOLIO] = [...payload]
  }
}

const actions = {
  [types.BUY_STOCK] (context: any, payload: PortfolioItem) {
    const stock = context.rootState.StocksModule[types.STOCKS].find(
      (s: Stock) => s.label === payload.stock
    )
    // Reduce funds
    context.dispatch(types.UPDATE_FUNDS, payload.quantity * stock.price * -1)

    // Check if stock doesn't already exists in portfolio
    const portfolioItem = context.state[types.PORTFOLIO].find(
      (p: PortfolioItem) => p.stock === payload.stock
    )

    const updatedPortfolio = [...context.state[types.PORTFOLIO]]

    if (portfolioItem) {
      portfolioItem.quantity += payload.quantity
      const index = context.state[types.PORTFOLIO].findIndex(
        (p: PortfolioItem) => p.stock === payload.stock
      )
      updatedPortfolio[index] = portfolioItem
    } else {
      updatedPortfolio.push(payload)
    }

    context.commit(types.SET_PORTFOLIO, updatedPortfolio)
  },

  [types.SELL_STOCK] (
    context: any,
    payload: { portfolioItem: PortfolioItem; quantity: number }
  ) {
    // Increase funds
    // Update stocks amount

    const stock = context.rootState.StocksModule[types.STOCKS].find(
      (s: Stock) => s.label === payload.portfolioItem.stock
    )
    context.dispatch(
      types.UPDATE_FUNDS,
      payload.portfolioItem.quantity * stock.price
    )

    const portfolio = [...context.state[types.PORTFOLIO]]
    const portfolioItem = context.state[types.PORTFOLIO].find(
      (p: PortfolioItem) => p.stock === payload.portfolioItem.stock
    )
    const index = context.state[types.PORTFOLIO].findIndex(
      (p: PortfolioItem) => p.stock === payload.portfolioItem.stock
    )

    portfolioItem.quantity -= payload.quantity
    portfolio[index] = portfolioItem

    context.commit(types.SET_PORTFOLIO, portfolio)
  },

  [types.QUERY_PORTFOLIO] (context: any) {
    db.collection('portfolio')
      .get()
      .then(querySnapshot => {
        const portfolio = querySnapshot.docs.map(doc => doc.data())
        context.commit(types.SET_PORTFOLIO, portfolio)
      })
  },
  [types.SAVE_PORTFOLIO]: firestoreAction((context: any) => {
    const portfolio = context.state[types.PORTFOLIO]

    if (portfolio.length === 0) return

    const batch = db.batch()
    const portfolioCollectionRef = db.collection('portfolio')

    portfolio.forEach((pi: PortfolioItem) => {
      const ref = portfolioCollectionRef.doc(pi.stock)
      batch.set(ref, pi)
    })

    return batch.commit().then()
  })
}

export default {
  state,
  getters,
  mutations,
  actions
}
