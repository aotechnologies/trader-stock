import { Stock } from './stock.model'

export interface PortfolioItem {
  stock: string
  quantity: number
}
