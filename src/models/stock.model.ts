export interface Stock {
  label: string
  price: number
}
