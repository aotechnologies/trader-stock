import firebase from 'firebase/app'
import 'firebase/firestore'

export const db = firebase
  .initializeApp({
    apiKey: 'AIzaSyBUfvtarfIjDHfw7S8oIcdtWaOwTl4UN8g',
    authDomain: 'flutter-firebase-8e303.firebaseapp.com',
    databaseURL: 'https://flutter-firebase-8e303.firebaseio.com',
    projectId: 'flutter-firebase-8e303',
    storageBucket: 'flutter-firebase-8e303.appspot.com',
    messagingSenderId: '486611062821',
    appId: '1:486611062821:web:50176111a02191d083afcc'
  })
  .firestore()

const { Timestamp, GeoPoint } = firebase.firestore
export { Timestamp, GeoPoint }
