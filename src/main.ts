import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Toasted from 'vue-toasted'

Vue.config.productionTip = false

Vue.use(Toasted, {
  duration: 3000,
  position: 'bottom-center',
  theme: 'bubble'
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
